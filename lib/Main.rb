# frozen_string_literal: true

require_relative 'csv_calc'

class Main
  def self.init_file
    puts 'Введите имя файла (.csv):'
    filepath = gets.chomp
    CSVcalc.new(filepath)
  end

  def self.ch_cols(data, cols)
    count = 1
    (0..cols).each do |i|
      puts "#{count}"'. '"#{data[0][i]}"
      count += 1
    end
    gets.chomp.to_i
  end

  def self.prepare_data(csv, cols)
    buffer = []
    (1..csv.rows).each do |i|
      buffer[i - 1] = csv.data[i][cols].to_f
    end
    buffer
  end

  def self.start
    csv = init_file
    puts 'Введите столбцы для расчета'
    col = ch_cols(csv.data, csv.cols) - 1
    array = prepare_data(csv, col)
    puts 'Минимальное = '"#{csv.min(array)}"
    puts 'Максимальное = '"#{csv.max(array)}"
    puts 'Среднее = '"#{csv.mean(array)}"
    puts 'Дисперсия = '"#{csv.sample_variance(array)}"
  end
end

Main.start
