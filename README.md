# Лабораторная работа 2
Программа вычисляет статистические параметры (максимальное, минимальное, среднее значения, исправленную выборчную диспесию) для таблицы статистических данных взятых с сайта: https://digital.gov.ru/opendata/  

```bash
gem install bundler
bundle install
```

## Tests

```bash
rspec
```

## Rubocop

```bash
rubocop
rubocop -a # Auto-correct
